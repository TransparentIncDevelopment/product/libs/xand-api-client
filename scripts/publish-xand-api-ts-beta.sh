#!/usr/bin/env bash

SCRIPT_DIR="$(dirname ${BASH_SOURCE[0]})"

pushd xand-api-ts-client
VERSION=$(node -p "require('./package.json').version")
JOB_VERSION="${VERSION}-beta.${CI_JOB_ID}"
npm --no-git-tag-version version $JOB_VERSION
yarn install
yarn build
npm publish --tag beta
popd

#!/usr/bin/env bash    
    
set -e

pushd "./xand-api-proto"
CURRENT_VERSION=$(toml get Cargo.toml package.version | tr -d '"')
BETA_VERSION="$CURRENT_VERSION-beta.${CI_PIPELINE_IID}"
echo "Setting beta version in Cargo.toml -> $BETA_VERSION"
NEW_CARGO_TOML=$(toml set Cargo.toml package.version $BETA_VERSION)
echo "$NEW_CARGO_TOML" > Cargo.toml
cargo publish --allow-dirty
popd

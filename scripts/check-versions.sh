#!/usr/bin/env bash

set -e

SCRIPT_DIR="$(dirname ${BASH_SOURCE[0]})"

if ! which toml > /dev/null; then
    echo "ERROR: missing toml-cli rust utility"
    exit 1
fi

function get_crate_version() {
    local -r crate_dir="${SCRIPT_DIR}/../$1"
    toml get "${crate_dir}/Cargo.toml" package.version | sed 's/\"//g'
}

export PROTO_VERSION="$(get_crate_version ./xand-api-proto)"
export RUST_CLIENT_VERSION="$(get_crate_version ./xand-api-client)"
export TS_CLIENT_VERSION="$(cat "${SCRIPT_DIR}/../xand-api-ts-client/package.json" | jq -r .version)"

if [ "$RUST_CLIENT_VERSION" != "$PROTO_VERSION" ]; then
    echo "ERROR: crate versions do not match"
    exit 1
fi

if [ "$TS_CLIENT_VERSION" != "$PROTO_VERSION" ]; then
    echo "ERROR: typescript client's version does not match xand-api-proto crate's version"
    exit 1
fi

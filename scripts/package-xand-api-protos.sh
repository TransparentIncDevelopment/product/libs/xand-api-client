#!/usr/bin/env bash

export PROPERTIES="job_url=$CI_JOB_URL;branch=$CI_COMMIT_REF_NAME;pipeline_id=$CI_PIPELINE_IID"
export VERSION="$(toml get ./xand-api-proto/Cargo.toml package.version | sed 's/\"//g')"

pushd xand-api-proto
export ZIP_FILENAME="xand-api-proto-${VERSION}.zip"
zip -R $ZIP_FILENAME '*.proto'

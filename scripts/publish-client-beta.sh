#!/usr/bin/env bash    
    
set -e

pushd "./xand-api-client"
CURRENT_VERSION=$(toml get Cargo.toml package.version | tr -d '"')
BETA_VERSION="$CURRENT_VERSION-beta.${CI_PIPELINE_IID}"
echo "Setting beta version in Cargo.toml -> $BETA_VERSION"
NEW_CARGO_TOML=$(toml set Cargo.toml package.version $BETA_VERSION)
echo "$NEW_CARGO_TOML" > Cargo.toml
echo "Setting xand-api-proto dependency version in Cargo.toml -> $BETA_VERSION"
BETA_PROTO_VERSION=$(toml set Cargo.toml dependencies.xand-api-proto.version $BETA_VERSION)
echo "$BETA_PROTO_VERSION" > Cargo.toml
cargo publish --allow-dirty
popd

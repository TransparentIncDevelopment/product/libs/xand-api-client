#!/usr/bin/env bash    
    
set -e

export CRATE_DIR="$1"

pushd "$CRATE_DIR"
cargo publish
popd

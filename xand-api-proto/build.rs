#![forbid(unsafe_code)]

fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::compile_protos("./xand-api.proto")?;
    Ok(())
}

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## Unreleased
<!-- When a new release is made, update tags and the URL hyperlinking the diff, see end of document -->
<!-- You may also wish to list updates that are made via MRs but not yet cut as part of a release -->
Section intentionally left blank.

<!-- When a new release is made, add it here
e.g. ## [0.8.2] - 2020-11-09 -->

## [48.0.0] - 2022-10-25
### Modified
- Get/Set PendingCreateRequestExpireTime now returns/takes milliseconds instead of blocks

## [47.0.0] - 2022-09-27
### Modified
- Health check endpoint returns structure with more information: elapsed time since startup, elapsed time since last block, best known block, current block

## [46.0.0] - 2022-09-27
### Added
- Health check endpoint to XandApi client/server
### Removed
- Separate HealthCheck client/server

## [45.0.0] - 2022-09-02
### Modified
- `Unkown` Health Check changed to `Unknown`

## [44.1.0] - 2022-08-29
### Added
- WithdrawFromNetwork

## [44.0.0] - 2022-06-06
### Added
- `AdminTransaction::RuntimeUpgrade`
### Removed
- `AdminTransaction::SetCode`

## [42.0.0] - 2022-04-20
### Nomenclature updates to several items:
- Payment (message) -> Send
- payment (any transaction) -> send
- payment (operation) -> send
- PAYMENT (transaction type) -> SEND
- Transfer (xandtransaction) -> Send
- Transfer (struct) -> Send
- GetWhitelist -> GetAllowlist
- WhitelistRequest -> AllowlistRequest
- Whitelist -> Allowlist
- WhitelistEntry -> AllowlistEntry
- /v1/whitelist -> /v1/allowlist
- WhitelistCidrBlock -> AllowlistCidrBlock
- whitelist_cidr_block (user transaction operation) -> allowlist_cidr_block
- RemoveWhitelistCidrBlock -> RemoveAllowlistCidrBlock
- RootWhitelistCidrBlock -> RootAllowlistCidrBlock
- root_whitelist_cidr_block (administrative transaction operation) -> root_allowlist_cidr_block
- RootRemoveWhitelistCidrBlock -> RootRemoveAllowlistCidrBlock
- root_remove_whitelist_cidr_block (administrative transaction operation) -> root_remove_allowlist_cidr_block
- whitelist_cidr_block (any transaction operation) -> allowlist_cidr_block
- root_whitelist_cidr_block (any transaction operation) -> root_allowlist_cidr_block
- root_remove_whitelist_cidr_block (any transaction operation) -> root_remove_allowlist_cidr_block
- WHITELIST_CIDR_BLOCK (transaction type) -> ALLOWLIST_CIDR_BLOCK
- ROOT_WHITELIST_CIDR_BLOCK (transaction type) -> ROOT_ALLOWLIST_CIDR_BLOCK
- ROOT_REMOVE_WHITELIST_CIDR_BLOCK (transaction type) -> ROOT_REMOVE_ALLOWLIST_CIDR_BLOCK

## [41.0.0] - 2022-04-20
### Nomenclature updates to several items:
- nonce -> correlation id
- council/council voting -> network/network voting
- /v1/creations(endpoint) -> /v1/pending-create-requests
- SubmittedCreation(message) -> CreateRequestPair
- submitted_creation(field) -> create_request
- SubmittedCreation(transaction type)-> CREATE_REQUEST
- PendingCreation -> PendingCreateRequest 
- SetPendingCreateRequestExpire -> SetPendingCreateRequestExpire
- PendingCreations -> PendingCreateRequests 
- PendingCreationsRequest -> PendingCreateRequestsPagination 
- CreationRequest -> CreateRequest 
- Creation -> CashConfirmation 
- CancelPendingCreate -> CreateCancellation 
- CreationCompletion -> CreateRequestCompletion
- PendingMint -> PendingCreateRequest
- CreatePendingMint (xand transaction) -> CreateRequest
- FulfillMint (xand transaction) -> CashConfirmation
- FulfillPendingMint (struct) -> CashConfirmation
- CancelMint (xand transaction) -> CreateCancellation
- CancelPendingMint (struct) -> CreateCancellation 
- SetPMintExpire (xand transaction) -> SetPendingCreateRequestExpire
- SetPendingMintExpire (struct) -> SetPendingCreateRequestExpire
- PendingSmelt -> PendingRedeemRequest
- CreatePendingSmelt (xand transaction) -> RedeemRequest
- FulfillSmelt (xand transaction) -> RedeemFulfillment
- FulfillPendingSmelt (struct) -> RedeemFulfillment
- CancelPendingSmelt (struct) -> RedeemCancellation
- CancelSmelt (xand transaction) -> RedeemCancellation
- PendingRedemptionsRequest -> PendingRedeemRequestsPagination
- CancelPendingRedeem -> RedeemCancellation
- GetPendingRedemptions -> GetPendingRedeemRequests
- PendingRedemptions -> PendingRedeemRequests
- PendingRedemption -> PendingRedeemRequest
- Redemption -> RedeemRequest
- RedemptionCompletion -> RedeemRequestCompletion
- RedemptionFulfillment -> RedeemFulfillment
- SubmittedRedemption(message) -> RedeemRequestPair
- submitted_redemption(any transaction) -> redeem_request
- redemption(user transation) -> redeem_request
- SUBMITTED_REDEMPTION (transaction type) -> REDEEM_REQUEST
- REDEMPTION_FULFILLMENT (transaction type) -> REDEEM_FULFILLMENT
- CANCEL_PENDING_REDEEM (transaction type) -> REDEEM_CANCELLATION
- /v1/redemptions (endpoint) -> /v1/pending-redeem-requests
- pending_redemptions (member) -> pending_redeem_requests

## [40.0.0] - 2022-03-23
### Removed
- `block_id` field removed from Committed and Finalized `TransactionStatus` variants.

## [39.0.0] - 2022-03-17
### Changed
- `Expiration` variant now contains a `TransactionId`

## [38.0.0] - 2022-03-16
### Removed
- `InsufficientFunds` variant removed from `RedemptionCancelReason`

## [37.0.0] - 2022-03-02
### Removed
- Separate ValidatorRedemption endpoints (`ValidatorRedemption`, `ValidatorRedemptionFulfillment`, `CancelPendingValidatorRedeem`)

## [36.0.0] - 2022-02-28
### Removed
- `RewardTransaction` variant from `AnyTransaction`

## [35.0.0] - 2022-02-24
### Added
- `RewardTransaction` variant to `AnyTransaction`

## [34.0.1] - 2022-02-17
### Added
- Crate is subscribed to ci auto-updates
- Update to Rust 1.58

## [34.0.0] - 2022-02-03
### Added
- Transactions to submit(`ValidatorRedemption`), fulfill(`ValidatorRedemptionFulfillment`), and cancel(`CancelPendingValidatorRedeem`) a validator redemption request

## [33.0.0] - 2021-10-12
### Changed
- `XandTransaction` has been reverted to be an exhaustive enum once more

## [32.0.0] - 2021-09-28

### Added
- `AnyTransaction` transaction `ExitMember`

### Changed
- Annotations for `RemoveMember` updated to "mark member for removal" (whereas `ExitMember` immediately removes a member)

## [9.0.0] - 2021-01-29

### Added
- Crate's own models: `xand_api_proto::proto_models`

### Changed
- `BankAccountInfo` struct on the `xand-api-client` protobuffer surface has changed: `Encrypted` variant now contains an error payload (rather than encrypted bytes).

### Removed
- Removing `nonce_to_string` from all Xand API Proto `xand-models`; consumers should replace with `xand_api_proto::xand_models::Nonce`, and its `to_string()` where needed.
- Xandstrate adapters and Substrate dependent conversions moved to `xand-api`.
- No more Substrate dependencies remain in crate (only Public Key cryptographic primitive dependencies such as `curve25519-dalek` remain)





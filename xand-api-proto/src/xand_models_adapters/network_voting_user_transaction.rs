use super::*;
use crate::proto_models::{
    Proposal as XmProposal, ProposalStage, SubmitProposal as XmSubmitProposal,
    VoteProposal as XmVoteProposal,
};
use crate::{error::XandApiProtoErrs, proposal::ProposalStatus, VoteProposal};
use std::{collections::HashMap, convert::TryInto};
use xand_address::Address;

impl From<XmSubmitProposal> for SubmitProposal {
    fn from(p: XmSubmitProposal) -> SubmitProposal {
        let inner = p.proposed_action;
        let issuer = "".to_string();
        let operation = Some(inner.into());
        let txn = AdministrativeTransaction { operation };
        let proposed_action = Some(txn);
        SubmitProposal {
            issuer,
            proposed_action,
        }
    }
}

impl TryFrom<SubmitProposal> for XmSubmitProposal {
    type Error = XandApiProtoErrs;
    fn try_from(p: SubmitProposal) -> Result<Self, Self::Error> {
        let txn = p
            .proposed_action
            .ok_or(XandApiProtoErrs::ConversionError {
                reason: "Proposal has no proposed action".to_string(),
            })?
            .operation
            .ok_or(XandApiProtoErrs::ConversionError {
                reason: "Transaction has no operation".to_string(),
            })?;
        let proposed_action = txn.try_into()?;
        Ok(XmSubmitProposal { proposed_action })
    }
}

impl From<XmVoteProposal> for VoteProposal {
    fn from(v: XmVoteProposal) -> VoteProposal {
        VoteProposal {
            id: v.id,
            vote: v.vote,
        }
    }
}

impl From<VoteProposal> for XmVoteProposal {
    fn from(v: VoteProposal) -> Self {
        XmVoteProposal {
            id: v.id,
            vote: v.vote,
        }
    }
}

impl TryFrom<ProposalStatus> for ProposalStage {
    type Error = XandApiProtoErrs;

    fn try_from(status: ProposalStatus) -> Result<Self, Self::Error> {
        Ok(match status {
            ProposalStatus::Proposed => ProposalStage::Proposed,
            ProposalStatus::Accepted => ProposalStage::Accepted,
            ProposalStatus::Rejected => ProposalStage::Rejected,
            ProposalStatus::Invalid => ProposalStage::Invalid,
        })
    }
}

impl From<ProposalStage> for ProposalStatus {
    fn from(stage: ProposalStage) -> Self {
        match stage {
            ProposalStage::Proposed => ProposalStatus::Proposed,
            ProposalStage::Accepted => ProposalStatus::Accepted,
            ProposalStage::Rejected => ProposalStatus::Rejected,
            ProposalStage::Invalid => ProposalStatus::Invalid,
        }
    }
}

impl TryFrom<crate::Proposal> for XmProposal {
    type Error = XandApiProtoErrs;

    fn try_from(p: crate::Proposal) -> Result<Self, Self::Error> {
        let votes = p
            .votes
            .into_iter()
            .map(|(address, vote)| {
                let address = address.parse::<Address>().map_err(|e| {
                    let reason = format!("Invalid address {}: {}", address, e);
                    XandApiProtoErrs::ConversionError { reason }
                })?;
                Ok((address, vote))
            })
            .collect::<Result<HashMap<_, _>, XandApiProtoErrs>>()?;
        let proposed_action = p
            .proposed_action
            .ok_or_else(|| {
                let reason = "Missing proposed_action in Proposal".into();
                XandApiProtoErrs::ConversionError { reason }
            })?
            .try_into()?;
        let status = p.status;
        let status = ProposalStatus::from_i32(status)
            .ok_or_else(|| {
                let reason = format!("Unknown proposal status {}", status);
                XandApiProtoErrs::ConversionError { reason }
            })?
            .try_into()?;
        let p = XmProposal {
            id: p.id,
            votes,
            proposer: p.proposer.try_into()?,
            expiration_block_id: p.expiration_block_id,
            proposed_action,
            status,
        };
        Ok(p)
    }
}

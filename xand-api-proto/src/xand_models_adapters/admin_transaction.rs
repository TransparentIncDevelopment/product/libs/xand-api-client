use super::*;
use crate::error::XandApiProtoErrs;
use crate::proto_models::{
    AddAuthorityKey as XmAddAuthority, AdministrativeTransaction as XmAdminTransaction,
    RegisterAccountAsMember as XmRegisterMember, RemoveAuthorityKey as XMRemoveAuthority,
    RemoveMember as XMRemoveMember, RootAllowlistCidrBlock as XMRootCidr,
    RootRemoveAllowlistCidrBlock as XMRootRemoveCidr, RuntimeUpgrade as XMRuntimeUpgrade,
    SetLimitedAgentId, SetMemberEncKey as XMSetMemberEncKey, SetTrustEncKey as XMSetTrustEncKey,
    SetTrustNodeId, SetValidatorEmissionRate as XMSetValidatorEmissionRate, XandTransaction,
};

// TODO: To get an exhaustive mapping we will want to have a concept of admin txn in
//  xand-models (https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/4310)
impl TryFrom<admin_txn::Operation> for XandTransaction {
    type Error = XandApiProtoErrs;

    fn try_from(txn: admin_txn::Operation) -> Result<Self, Self::Error> {
        Ok(match txn {
            admin_txn::Operation::RegisterAccountAsMember(r) => {
                XandTransaction::RegisterMember(r.try_into()?)
            }
            admin_txn::Operation::RemoveMember(r) => XandTransaction::RemoveMember(r.try_into()?),
            admin_txn::Operation::SetTrust(s) => XandTransaction::SetTrust(s.try_into()?),
            admin_txn::Operation::SetLimitedAgent(a) => {
                XandTransaction::SetLimitedAgent(a.try_into()?)
            }
            admin_txn::Operation::SetValidatorEmissionRate(a) => {
                XandTransaction::SetValidatorEmissionRate(a.into())
            }
            admin_txn::Operation::AddAuthorityKey(a) => {
                XandTransaction::AddAuthorityKey(a.try_into()?)
            }
            admin_txn::Operation::RemoveAuthorityKey(r) => {
                XandTransaction::RemoveAuthorityKey(r.try_into()?)
            }
            admin_txn::Operation::RootAllowlistCidrBlock(r) => {
                XandTransaction::RootAllowlistCidrBlock(r.try_into()?)
            }
            admin_txn::Operation::RootRemoveAllowlistCidrBlock(r) => {
                XandTransaction::RootRemoveAllowlistCidrBlock(r.try_into()?)
            }
            admin_txn::Operation::RuntimeUpgrade(r) => {
                XandTransaction::RuntimeUpgrade(r.try_into()?)
            }
        })
    }
}

// TODO: To get an exhaustive mapping we will want to have a concept of admin txn in
//  xand-models (https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/4310)
impl TryFrom<XandTransaction> for admin_txn::Operation {
    type Error = XandApiProtoErrs;

    fn try_from(txn: XandTransaction) -> Result<Self, Self::Error> {
        Ok(match txn {
            XandTransaction::RegisterMember(r) => {
                admin_txn::Operation::RegisterAccountAsMember(r.into())
            }
            XandTransaction::RemoveMember(r) => admin_txn::Operation::RemoveMember(r.into()),
            XandTransaction::SetTrust(s) => admin_txn::Operation::SetTrust(s.into()),
            XandTransaction::AddAuthorityKey(a) => admin_txn::Operation::AddAuthorityKey(a.into()),
            XandTransaction::RemoveAuthorityKey(r) => {
                admin_txn::Operation::RemoveAuthorityKey(r.into())
            }
            XandTransaction::RootAllowlistCidrBlock(r) => {
                admin_txn::Operation::RootAllowlistCidrBlock(r.into())
            }
            XandTransaction::RootRemoveAllowlistCidrBlock(r) => {
                admin_txn::Operation::RootRemoveAllowlistCidrBlock(r.into())
            }
            XandTransaction::SetLimitedAgent(la) => {
                admin_txn::Operation::SetLimitedAgent(la.into())
            }
            XandTransaction::SetValidatorEmissionRate(a) => {
                admin_txn::Operation::SetValidatorEmissionRate(a.into())
            }
            XandTransaction::RuntimeUpgrade(r) => admin_txn::Operation::RuntimeUpgrade(r.try_into().expect("A RuntimeUpgrade.xand_hash should never error while being serialized into bytes")),
            XandTransaction::Send(_)
            | XandTransaction::SetMemberEncryptionKey(_)
            | XandTransaction::SetTrustEncryptionKey(_)
            | XandTransaction::CreateRequest(_)
            | XandTransaction::RedeemRequest(_)
            | XandTransaction::CashConfirmation(_)
            | XandTransaction::RedeemFulfillment(_)
            | XandTransaction::CreateCancellation(_)
            | XandTransaction::RedeemCancellation(_)
            | XandTransaction::AllowlistCidrBlock(_)
            | XandTransaction::RemoveAllowlistCidrBlock(_)
            | XandTransaction::SetPendingCreateRequestExpire(_)
            | XandTransaction::SubmitProposal(_)
            | XandTransaction::VoteProposal(_)
            | XandTransaction::RegisterSessionKeys(_)
            | XandTransaction::WithdrawFromNetwork(_)
            | XandTransaction::ExitMember(_) => {
                return Err(XandApiProtoErrs::ConversionError {
                    reason: format!(
                        "Proposed action {:?} is not an Administrative Transaction",
                        txn
                    ),
                })
            }
        })
    }
}

impl From<XmRegisterMember> for xand_api::RegisterAccountAsMember {
    fn from(r: XmRegisterMember) -> Self {
        Self {
            address: r.address.to_string(),
            encryption_key: r.encryption_key.as_bytes().to_vec(),
        }
    }
}

impl TryFrom<xand_api::RegisterAccountAsMember> for XmRegisterMember {
    type Error = XandApiProtoErrs;
    fn try_from(x: xand_api::RegisterAccountAsMember) -> Result<Self, Self::Error> {
        Ok(Self {
            address: x.address.parse()?,
            encryption_key: x.encryption_key.try_into()?,
        })
    }
}

impl From<XMRemoveMember> for xand_api::RemoveMember {
    fn from(r: XMRemoveMember) -> Self {
        Self {
            address: r.address.to_string(),
        }
    }
}

impl TryFrom<xand_api::RemoveMember> for XMRemoveMember {
    type Error = XandApiProtoErrs;

    fn try_from(r: xand_api::RemoveMember) -> Result<Self, Self::Error> {
        Ok(Self {
            address: r.address.parse()?,
        })
    }
}

impl From<SetTrustNodeId> for SetTrust {
    fn from(s: SetTrustNodeId) -> Self {
        SetTrust {
            address: s.address.to_string(),
            encryption_key: s.encryption_key.as_bytes().to_vec(),
        }
    }
}
impl TryFrom<SetTrust> for SetTrustNodeId {
    type Error = XandApiProtoErrs;
    fn try_from(x: SetTrust) -> Result<Self, Self::Error> {
        Ok(SetTrustNodeId {
            address: x.address.parse()?,
            encryption_key: x.encryption_key.try_into()?,
        })
    }
}

impl From<SetLimitedAgentId> for SetLimitedAgent {
    fn from(s: SetLimitedAgentId) -> Self {
        SetLimitedAgent {
            address: s.address.map(|a| OptionalAddress {
                address_str: a.to_string(),
            }),
        }
    }
}
impl TryFrom<SetLimitedAgent> for SetLimitedAgentId {
    type Error = XandApiProtoErrs;
    fn try_from(x: SetLimitedAgent) -> Result<Self, Self::Error> {
        Ok(SetLimitedAgentId {
            address: x
                .address
                .map(|OptionalAddress { address_str }| address_str.parse())
                .transpose()?,
        })
    }
}

impl From<XMSetValidatorEmissionRate> for SetValidatorEmissionRate {
    fn from(s: XMSetValidatorEmissionRate) -> Self {
        SetValidatorEmissionRate {
            minor_units_per_emission: s.minor_units_per_emission,
            block_quota: s.block_quota,
        }
    }
}
impl From<SetValidatorEmissionRate> for XMSetValidatorEmissionRate {
    fn from(x: SetValidatorEmissionRate) -> Self {
        XMSetValidatorEmissionRate {
            minor_units_per_emission: x.minor_units_per_emission,
            block_quota: x.block_quota,
        }
    }
}

impl From<XMSetMemberEncKey> for SetMemberEncryptionKey {
    fn from(s: XMSetMemberEncKey) -> Self {
        SetMemberEncryptionKey {
            encryption_key: s.key.as_bytes().to_vec(),
        }
    }
}
impl TryFrom<SetMemberEncryptionKey> for XMSetMemberEncKey {
    type Error = XandApiProtoErrs;
    fn try_from(x: SetMemberEncryptionKey) -> Result<Self, Self::Error> {
        Ok(XMSetMemberEncKey {
            key: x.encryption_key.try_into()?,
        })
    }
}
impl From<XMSetTrustEncKey> for SetTrustEncryptionKey {
    fn from(s: XMSetTrustEncKey) -> Self {
        SetTrustEncryptionKey {
            encryption_key: s.key.as_bytes().to_vec(),
        }
    }
}
impl TryFrom<SetTrustEncryptionKey> for XMSetTrustEncKey {
    type Error = XandApiProtoErrs;
    fn try_from(x: SetTrustEncryptionKey) -> Result<Self, Self::Error> {
        Ok(XMSetTrustEncKey {
            key: x.encryption_key.try_into()?,
        })
    }
}

impl From<XmAddAuthority> for AddAuthorityKey {
    fn from(a: XmAddAuthority) -> Self {
        AddAuthorityKey {
            address: a.account_id.to_string(),
        }
    }
}
impl TryFrom<AddAuthorityKey> for XmAddAuthority {
    type Error = XandApiProtoErrs;
    fn try_from(x: AddAuthorityKey) -> Result<Self, Self::Error> {
        Ok(XmAddAuthority {
            account_id: x.address.parse()?,
        })
    }
}

impl From<XMRemoveAuthority> for RemoveAuthorityKey {
    fn from(r: XMRemoveAuthority) -> Self {
        RemoveAuthorityKey {
            address: r.account_id.to_string(),
        }
    }
}
impl TryFrom<RemoveAuthorityKey> for XMRemoveAuthority {
    type Error = XandApiProtoErrs;
    fn try_from(x: RemoveAuthorityKey) -> Result<Self, Self::Error> {
        Ok(XMRemoveAuthority {
            account_id: x.address.parse()?,
        })
    }
}

impl From<XMRootCidr> for RootAllowlistCidrBlock {
    fn from(r: XMRootCidr) -> Self {
        RootAllowlistCidrBlock {
            cidr_block: r.cidr_block.to_string(),
            address: r.account.to_string(),
        }
    }
}
impl TryFrom<RootAllowlistCidrBlock> for XMRootCidr {
    type Error = XandApiProtoErrs;
    fn try_from(x: RootAllowlistCidrBlock) -> Result<Self, Self::Error> {
        Ok(XMRootCidr {
            cidr_block: x.cidr_block.parse()?,
            account: x.address.parse()?,
        })
    }
}

impl From<XMRootRemoveCidr> for RootRemoveAllowlistCidrBlock {
    fn from(r: XMRootRemoveCidr) -> Self {
        Self {
            cidr_block: r.cidr_block.to_string(),
            address: r.account.to_string(),
        }
    }
}
impl TryFrom<RootRemoveAllowlistCidrBlock> for XMRootRemoveCidr {
    type Error = XandApiProtoErrs;
    fn try_from(x: RootRemoveAllowlistCidrBlock) -> Result<Self, Self::Error> {
        Ok(Self {
            cidr_block: x.cidr_block.parse()?,
            account: x.address.parse()?,
        })
    }
}

impl TryFrom<RuntimeUpgrade> for XMRuntimeUpgrade {
    type Error = XandApiProtoErrs;
    fn try_from(r: RuntimeUpgrade) -> Result<Self, Self::Error> {
        Ok(Self {
            code: r.code,
            xand_hash: bincode::deserialize(&r.xand_hash)?,
            wait_blocks: r.wait_blocks,
        })
    }
}

impl TryFrom<XMRuntimeUpgrade> for RuntimeUpgrade {
    type Error = Box<bincode::ErrorKind>;
    fn try_from(x: XMRuntimeUpgrade) -> Result<Self, Self::Error> {
        Ok(RuntimeUpgrade {
            code: x.code,
            xand_hash: bincode::serialize(&x.xand_hash)?,
            wait_blocks: x.wait_blocks,
        })
    }
}

impl TryFrom<admin_txn::Operation> for XmAdminTransaction {
    type Error = XandApiProtoErrs;

    fn try_from(op: admin_txn::Operation) -> Result<Self, Self::Error> {
        let txn = match op {
            admin_txn::Operation::RegisterAccountAsMember(x) => {
                XmAdminTransaction::RegisterAccountAsMember(x.try_into()?)
            }
            admin_txn::Operation::RemoveMember(x) => {
                XmAdminTransaction::RemoveMember(x.try_into()?)
            }
            admin_txn::Operation::SetTrust(x) => XmAdminTransaction::SetTrust(x.try_into()?),
            admin_txn::Operation::SetLimitedAgent(x) => {
                XmAdminTransaction::SetLimitedAgent(x.try_into()?)
            }
            admin_txn::Operation::SetValidatorEmissionRate(x) => {
                XmAdminTransaction::SetValidatorEmissionRate(x.into())
            }
            admin_txn::Operation::AddAuthorityKey(x) => {
                XmAdminTransaction::AddAuthorityKey(x.try_into()?)
            }
            admin_txn::Operation::RemoveAuthorityKey(x) => {
                XmAdminTransaction::RemoveAuthorityKey(x.try_into()?)
            }
            admin_txn::Operation::RootAllowlistCidrBlock(x) => {
                XmAdminTransaction::RootAllowlistCidrBlock(x.try_into()?)
            }
            admin_txn::Operation::RootRemoveAllowlistCidrBlock(x) => {
                XmAdminTransaction::RootRemoveAllowlistCidrBlock(x.try_into()?)
            }
            admin_txn::Operation::RuntimeUpgrade(x) => {
                XmAdminTransaction::RuntimeUpgrade(x.try_into()?)
            }
        };
        Ok(txn)
    }
}

impl From<XmAdminTransaction> for admin_txn::Operation {
    fn from(txn: XmAdminTransaction) -> Self {
        match txn {
            XmAdminTransaction::RegisterAccountAsMember(x) => {
                admin_txn::Operation::RegisterAccountAsMember(x.into())
            }
            XmAdminTransaction::RemoveMember(x) => admin_txn::Operation::RemoveMember(x.into()),
            XmAdminTransaction::SetTrust(x) => admin_txn::Operation::SetTrust(x.into()),
            XmAdminTransaction::SetLimitedAgent(x) => {
                admin_txn::Operation::SetLimitedAgent(x.into())
            }
            XmAdminTransaction::SetValidatorEmissionRate(x) => {
                admin_txn::Operation::SetValidatorEmissionRate(x.into())
            }
            XmAdminTransaction::AddAuthorityKey(x) => {
                admin_txn::Operation::AddAuthorityKey(x.into())
            }
            XmAdminTransaction::RemoveAuthorityKey(x) => {
                admin_txn::Operation::RemoveAuthorityKey(x.into())
            }
            XmAdminTransaction::RootAllowlistCidrBlock(x) => {
                admin_txn::Operation::RootAllowlistCidrBlock(x.into())
            }
            XmAdminTransaction::RootRemoveAllowlistCidrBlock(x) => {
                admin_txn::Operation::RootRemoveAllowlistCidrBlock(x.into())
            }
            XmAdminTransaction::RuntimeUpgrade(x) => admin_txn::Operation::RuntimeUpgrade(x.try_into().expect("A RuntimeUpgrade.xand_hash should never error while being serialized into bytes")),
        }
    }
}

impl TryFrom<AdministrativeTransaction> for XmAdminTransaction {
    type Error = XandApiProtoErrs;

    fn try_from(t: AdministrativeTransaction) -> Result<Self, Self::Error> {
        let op = t.operation.ok_or_else(|| {
            let reason = "Missing operation in AdministrativeTransaction".into();
            XandApiProtoErrs::ConversionError { reason }
        })?;
        op.try_into()
    }
}

impl From<XmAdminTransaction> for AdministrativeTransaction {
    fn from(txn: XmAdminTransaction) -> Self {
        AdministrativeTransaction {
            operation: Some(txn.into()),
        }
    }
}

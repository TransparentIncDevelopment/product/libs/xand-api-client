use crate::{
    error::XandApiProtoErrs, proto_models::HealthResponse as XMHealthResponse,
    proto_models::HealthStatus as XMHealthStatus, HealthCheckResponse,
};

impl TryFrom<i32> for XMHealthStatus {
    type Error = XandApiProtoErrs;

    fn try_from(status: i32) -> Result<Self, Self::Error> {
        match status {
            0 => Ok(XMHealthStatus::Unhealthy),
            1 => Ok(XMHealthStatus::Healthy),
            2 => Ok(XMHealthStatus::Syncing),
            other => Err(XandApiProtoErrs::ConversionError {
                reason: format!("Unknown health status code: {}", other),
            }),
        }
    }
}

impl TryFrom<HealthCheckResponse> for XMHealthResponse {
    type Error = XandApiProtoErrs;

    fn try_from(resp: HealthCheckResponse) -> Result<Self, Self::Error> {
        Ok(Self {
            status: resp.status.try_into()?,
            current_block: resp.current_block,
            elapsed_blocks_since_startup: resp.elapsed_blocks_since_startup,
            elapsed_time_since_startup_millis: resp.elapsed_time_since_startup_millis,
            best_known_block: resp.best_known_block,
        })
    }
}

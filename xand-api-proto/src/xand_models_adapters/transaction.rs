use super::*;

use crate::proto_models::{
    ExitMember as XMExitMember, TotalIssuance, Transaction as XMTransaction,
    TransactionStatus as XMTransactionStatus, TransactionType as XMTransactionType,
};
use crate::{error::XandApiProtoErrs, xand_api::*};

impl TryFrom<FetchedTransaction> for proto_models::Transaction {
    type Error = XandApiProtoErrs;

    fn try_from(t: FetchedTransaction) -> Result<Self, Self::Error> {
        let extracted_tx = t
            .transaction
            .ok_or_else(|| XandApiProtoErrs::ConversionError {
                reason: "Fetched transaction missing transaction field".to_string(),
            })?;
        let converted_tx: XandTransaction = extracted_tx
            .operation
            .ok_or_else(|| XandApiProtoErrs::ConversionError {
                reason: "Fetched transaction missing inner operation field".to_string(),
            })?
            .try_into()?;
        let converted_status = t
            .status
            .ok_or_else(|| XandApiProtoErrs::ConversionError {
                reason: "Fetched transaction missing status field".to_string(),
            })?
            .status
            .ok_or_else(|| XandApiProtoErrs::ConversionError {
                reason: "Fetched transaction missing inner status field".to_string(),
            })?
            .into();
        Ok(proto_models::Transaction {
            transaction_id: t.id.parse()?,
            signer_address: extracted_tx.issuer.parse()?,
            status: converted_status,
            txn: converted_tx,
            timestamp: XMTransaction::timestamp_from_unix_time_millis(t.unix_time_millis).ok_or(
                XandApiProtoErrs::ConversionError {
                    reason: "Unable to parse timestamp field on fetched transaction".to_string(),
                },
            )?,
        })
    }
}

impl From<proto_models::CreateRequestCompletion> for create_request_completion::Id {
    fn from(id: proto_models::CreateRequestCompletion) -> create_request_completion::Id {
        match id {
            proto_models::CreateRequestCompletion::Confirmation(id) => {
                create_request_completion::Id::Confirmation(id.to_string())
            }
            proto_models::CreateRequestCompletion::Cancellation(id) => {
                create_request_completion::Id::Cancellation(id.to_string())
            }
            proto_models::CreateRequestCompletion::Expiration(id) => {
                create_request_completion::Id::Expiration(id.to_string())
            }
        }
    }
}

impl From<proto_models::CreateRequestCompletion> for CreateRequestCompletion {
    fn from(id: proto_models::CreateRequestCompletion) -> Self {
        Self {
            id: Some(id.into()),
        }
    }
}

impl TryFrom<create_request_completion::Id> for proto_models::CreateRequestCompletion {
    type Error = XandApiProtoErrs;

    fn try_from(x: create_request_completion::Id) -> Result<Self, Self::Error> {
        let id = match x {
            create_request_completion::Id::Confirmation(id) => {
                proto_models::CreateRequestCompletion::Confirmation(id.parse()?)
            }
            create_request_completion::Id::Cancellation(id) => {
                proto_models::CreateRequestCompletion::Cancellation(id.parse()?)
            }
            create_request_completion::Id::Expiration(id) => {
                proto_models::CreateRequestCompletion::Expiration(id.parse()?)
            }
        };
        Ok(id)
    }
}

impl TryFrom<CreateRequestCompletion> for proto_models::CreateRequestCompletion {
    type Error = XandApiProtoErrs;

    fn try_from(x: CreateRequestCompletion) -> Result<Self, Self::Error> {
        x.id.ok_or_else(|| XandApiProtoErrs::ConversionError {
            reason: "Completing transaction missing id field".into(),
        })?
        .try_into()
    }
}

impl From<proto_models::RedeemRequestCompletion> for redeem_request_completion::Id {
    fn from(id: proto_models::RedeemRequestCompletion) -> redeem_request_completion::Id {
        match id {
            proto_models::RedeemRequestCompletion::Confirmation(id) => {
                redeem_request_completion::Id::Confirmation(id.to_string())
            }
            proto_models::RedeemRequestCompletion::Cancellation(id) => {
                redeem_request_completion::Id::Cancellation(id.to_string())
            }
        }
    }
}

impl From<proto_models::RedeemRequestCompletion> for RedeemRequestCompletion {
    fn from(id: proto_models::RedeemRequestCompletion) -> Self {
        Self {
            id: Some(id.into()),
        }
    }
}

impl TryFrom<redeem_request_completion::Id> for proto_models::RedeemRequestCompletion {
    type Error = XandApiProtoErrs;

    fn try_from(x: redeem_request_completion::Id) -> Result<Self, Self::Error> {
        let id = match x {
            redeem_request_completion::Id::Confirmation(id) => {
                proto_models::RedeemRequestCompletion::Confirmation(id.parse()?)
            }
            redeem_request_completion::Id::Cancellation(id) => {
                proto_models::RedeemRequestCompletion::Cancellation(id.parse()?)
            }
        };
        Ok(id)
    }
}

impl TryFrom<RedeemRequestCompletion> for proto_models::RedeemRequestCompletion {
    type Error = XandApiProtoErrs;

    fn try_from(x: RedeemRequestCompletion) -> Result<Self, Self::Error> {
        x.id.ok_or_else(|| XandApiProtoErrs::ConversionError {
            reason: "Completing transaction missing id field".into(),
        })?
        .try_into()
    }
}

impl From<XMExitMember> for xand_api::ExitMember {
    fn from(r: XMExitMember) -> Self {
        Self {
            address: r.address.to_string(),
        }
    }
}

impl TryFrom<xand_api::ExitMember> for XMExitMember {
    type Error = XandApiProtoErrs;

    fn try_from(r: xand_api::ExitMember) -> Result<Self, Self::Error> {
        Ok(Self {
            address: r.address.parse()?,
        })
    }
}

impl From<XMTransaction> for FetchedTransaction {
    fn from(t: XMTransaction) -> Self {
        FetchedTransaction {
            id: t.transaction_id.to_string(),
            transaction: Some(AnyTransaction {
                issuer: t.signer_address.to_string(),
                operation: Some(t.txn.into()),
            }),
            status: Some(t.status.into()),
            unix_time_millis: t.timestamp.timestamp_millis(),
        }
    }
}

impl From<transaction_status::Status> for XMTransactionStatus {
    fn from(ts: transaction_status::Status) -> Self {
        match ts {
            transaction_status::Status::Received(_) => XMTransactionStatus::Pending,
            transaction_status::Status::Accepted(_) => XMTransactionStatus::Pending,
            transaction_status::Status::Broadcast(_) => XMTransactionStatus::Pending,
            transaction_status::Status::Invalid(i) => XMTransactionStatus::Invalid(i.reason),
            transaction_status::Status::Dropped(_) => {
                XMTransactionStatus::Invalid("Dropped".to_string())
            }
            transaction_status::Status::Committed(_) => XMTransactionStatus::Committed,
            transaction_status::Status::Finalized(_) => XMTransactionStatus::Finalized,
        }
    }
}

impl From<XMTransactionStatus> for xand_api::TransactionStatus {
    fn from(ts: XMTransactionStatus) -> Self {
        // The xand models version needs more detail.
        xand_api::TransactionStatus {
            status: match ts {
                XMTransactionStatus::Pending => Some(TransactionReceived {}.into()),
                XMTransactionStatus::Committed => Some(TransactionCommitted {}.into()),
                XMTransactionStatus::Finalized => Some(TransactionFinalized {}.into()),
                XMTransactionStatus::Invalid(r) => Some(TransactionInvalid { reason: r }.into()),
                XMTransactionStatus::Unknown => None,
            },
        }
    }
}

impl From<TransactionReceived> for transaction_status::Status {
    fn from(x: TransactionReceived) -> Self {
        transaction_status::Status::Received(x)
    }
}

impl From<TransactionAccepted> for transaction_status::Status {
    fn from(x: TransactionAccepted) -> Self {
        transaction_status::Status::Accepted(x)
    }
}

impl From<TransactionBroadcast> for transaction_status::Status {
    fn from(x: TransactionBroadcast) -> Self {
        transaction_status::Status::Broadcast(x)
    }
}

impl From<TransactionInvalid> for transaction_status::Status {
    fn from(x: TransactionInvalid) -> Self {
        transaction_status::Status::Invalid(x)
    }
}

impl From<TransactionCommitted> for transaction_status::Status {
    fn from(x: TransactionCommitted) -> Self {
        transaction_status::Status::Committed(x)
    }
}

impl From<TransactionFinalized> for transaction_status::Status {
    fn from(x: TransactionFinalized) -> Self {
        transaction_status::Status::Finalized(x)
    }
}

impl From<TransactionDropped> for transaction_status::Status {
    fn from(x: TransactionDropped) -> Self {
        transaction_status::Status::Dropped(x)
    }
}

impl TryFrom<TotalIssuanceResponse> for TotalIssuance {
    type Error = XandApiProtoErrs;

    fn try_from(x: TotalIssuanceResponse) -> Result<Self, Self::Error> {
        Ok(Self {
            total_issued: x.balance,
            blockstamp: x
                .blockstamp
                .ok_or_else(|| XandApiProtoErrs::ConversionError {
                    reason: "Missing blockstamp".into(),
                })?
                .try_into()?,
        })
    }
}

impl From<PendingCreateRequestExpireTime> for u64 {
    fn from(a: PendingCreateRequestExpireTime) -> Self {
        a.expire_in_milliseconds as u64
    }
}

pub trait StatusExt {
    fn is_final(&self) -> bool;
    fn been_committed(&self) -> bool;
}

impl StatusExt for transaction_status::Status {
    fn is_final(&self) -> bool {
        matches!(
            self,
            transaction_status::Status::Finalized(_)
                | transaction_status::Status::Dropped(_)
                | transaction_status::Status::Invalid(_)
        )
    }

    fn been_committed(&self) -> bool {
        matches!(
            self,
            transaction_status::Status::Finalized(_) | transaction_status::Status::Committed(_)
        )
    }
}

impl From<XMTransactionType> for TransactionType {
    fn from(t: XMTransactionType) -> Self {
        match t {
            XMTransactionType::RegisterMember => TransactionType::RegisterAccountAsMember,
            XMTransactionType::RegisterSessionKeys => TransactionType::RegisterSessionKeys,
            XMTransactionType::RemoveMember => TransactionType::RemoveMember,
            XMTransactionType::ExitMember => TransactionType::ExitMember,
            XMTransactionType::SetLimitedAgent => TransactionType::SetLimitedAgent,
            XMTransactionType::SetValidatorEmissionRate => {
                TransactionType::SetValidatorEmissionRate
            }
            XMTransactionType::SetTrust => TransactionType::SetTrust,
            XMTransactionType::SetMemberEncryptionKey => TransactionType::SetMemberEncryptionKey,
            XMTransactionType::SetTrustEncryptionKey => TransactionType::SetTrustEncryptionKey,
            XMTransactionType::SetPendingCreateRequestExpire => {
                TransactionType::SetPendingCreateRequestExpire
            }
            XMTransactionType::Send => TransactionType::Send,
            XMTransactionType::CreateRequest => TransactionType::CreateRequest,
            XMTransactionType::CashConfirmation => TransactionType::CashConfirmation,
            XMTransactionType::CreateCancellation => TransactionType::CreateCancellation,
            XMTransactionType::RedeemCancellation => TransactionType::RedeemCancellation,
            XMTransactionType::RedeemRequest => TransactionType::RedeemRequest,
            XMTransactionType::RedeemFulfillment => TransactionType::RedeemFulfillment,
            XMTransactionType::AddAuthorityKey => TransactionType::AddAuthorityKey,
            XMTransactionType::RemoveAuthorityKey => TransactionType::RemoveAuthorityKey,
            XMTransactionType::AllowlistCidrBlock => TransactionType::AllowlistCidrBlock,
            XMTransactionType::RemoveAllowlistCidrBlock => TransactionType::RemoveCidrBlock,
            XMTransactionType::RootAllowlistCidrBlock => TransactionType::RootAllowlistCidrBlock,
            XMTransactionType::RootRemoveAllowlistCidrBlock => {
                TransactionType::RootRemoveAllowlistCidrBlock
            }
            XMTransactionType::SubmitProposal => TransactionType::SubmitProposal,
            XMTransactionType::VoteProposal => TransactionType::VoteProposal,
            XMTransactionType::RuntimeUpgrade => TransactionType::RuntimeUpgrade,
            XMTransactionType::WithdrawFromNetwork => TransactionType::WithdrawFromNetwork,
        }
    }
}

impl From<TransactionType> for XMTransactionType {
    fn from(t: TransactionType) -> Self {
        match t {
            TransactionType::RegisterAccountAsMember => XMTransactionType::RegisterMember,
            TransactionType::RegisterSessionKeys => XMTransactionType::RegisterSessionKeys,
            TransactionType::RemoveMember => XMTransactionType::RemoveMember,
            TransactionType::ExitMember => XMTransactionType::ExitMember,
            TransactionType::SetTrust => XMTransactionType::SetTrust,
            TransactionType::SetMemberEncryptionKey => XMTransactionType::SetMemberEncryptionKey,
            TransactionType::SetTrustEncryptionKey => XMTransactionType::SetTrustEncryptionKey,
            TransactionType::SetPendingCreateRequestExpire => {
                XMTransactionType::SetPendingCreateRequestExpire
            }
            TransactionType::Send => XMTransactionType::Send,
            TransactionType::CreateRequest => XMTransactionType::CreateRequest,
            TransactionType::CashConfirmation => XMTransactionType::CashConfirmation,
            TransactionType::CreateCancellation => XMTransactionType::CreateCancellation,
            TransactionType::RedeemCancellation => XMTransactionType::RedeemCancellation,
            TransactionType::RedeemRequest => XMTransactionType::RedeemRequest,
            TransactionType::RedeemFulfillment => XMTransactionType::RedeemFulfillment,
            TransactionType::AddAuthorityKey => XMTransactionType::AddAuthorityKey,
            TransactionType::RemoveAuthorityKey => XMTransactionType::RemoveAuthorityKey,
            TransactionType::AllowlistCidrBlock => XMTransactionType::AllowlistCidrBlock,
            TransactionType::RemoveCidrBlock => XMTransactionType::RemoveAllowlistCidrBlock,
            TransactionType::RootAllowlistCidrBlock => XMTransactionType::RootAllowlistCidrBlock,
            TransactionType::RootRemoveAllowlistCidrBlock => {
                XMTransactionType::RootRemoveAllowlistCidrBlock
            }
            TransactionType::SubmitProposal => XMTransactionType::SubmitProposal,
            TransactionType::VoteProposal => XMTransactionType::VoteProposal,
            TransactionType::SetLimitedAgent => XMTransactionType::SetLimitedAgent,
            TransactionType::SetValidatorEmissionRate => {
                XMTransactionType::SetValidatorEmissionRate
            }
            TransactionType::RuntimeUpgrade => XMTransactionType::RuntimeUpgrade,
            TransactionType::WithdrawFromNetwork => XMTransactionType::WithdrawFromNetwork,
        }
    }
}

use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use thiserror::Error;

#[derive(Clone, Debug, Error, Serialize, Deserialize)]
pub enum FixedSizeFieldError {
    #[error("Data source was too large for field's size: {size:?}")]
    DataTooLargeForField { size: usize },
}

#[derive(Clone, Debug, Error, Hash, Eq, PartialEq, Serialize, Deserialize)]
pub enum EncryptionError {
    #[error("Encryption key could not be found")]
    KeyNotFound,
    #[error("Encryption or decryption could not operate on malformed message")]
    MessageMalformed,
}

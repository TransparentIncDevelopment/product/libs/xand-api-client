//! Provides functionality extending the api client so that its connection status can largely
//! be ignored by the user.

use crate::{
    errors::XandApiClientError,
    log_events::LoggingEvent::ReconnectingClientError,
    models::{Paginated, Paging},
    TransactionHistoryPage, TransactionId, TransactionStatusStream, XandApiClient,
    XandApiClientTrait,
};
use futures::{future::BoxFuture, FutureExt};
use std::fmt::Debug;
use std::ops::Deref;
use tokio::sync::{RwLock, RwLockReadGuard};
use url::Url;
use xand_address::Address;
use xand_api_proto::proto_models::{
    AdministrativeTransaction, Blockstamp, CidrBlock, HealthResponse, PendingCreateRequest,
    PendingRedeemRequest, Proposal, TotalIssuance, Transaction, TransactionFilter,
    ValidatorEmissionProgress, ValidatorEmissionRate, VoteProposal, XandTransaction,
};

/// A wrapper around something async that will re-attempt initialization automatically
pub struct Resurrectable<T, Err> {
    inner: RwLock<Option<T>>,
    initializer: Box<dyn Fn() -> BoxFuture<'static, Result<T, Err>> + Send + Sync>,
}

// This is just a way to provide a view *inside* the option. The option *must* be populated
// upon insertion
pub struct LockHolder<'a, T> {
    guard: RwLockReadGuard<'a, Option<T>>,
}
impl<'a, T> Deref for LockHolder<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.guard.as_ref().unwrap()
    }
}

impl<T, Err> Resurrectable<T, Err>
where
    Err: Debug,
{
    /// Construct a new resurrectable by passing a function that can instantiate the inner type.
    /// If instantiation fails, the resurrectable will still be created, but the first call to
    /// `get` will re-run the initializer.
    pub async fn new<Fun: Fn() -> BoxFuture<'static, Result<T, Err>> + Send + Sync + 'static>(
        initializer: Fun,
    ) -> Self {
        Self {
            inner: RwLock::new(initializer().await.ok()),
            initializer: Box::new(initializer),
        }
    }

    /// Construct a new resurrectable by passing a function that can instantiate the inner type.
    /// The function will *not* be called immediately. It will be called on the first call to `get`
    pub fn stub<Fun: Fn() -> BoxFuture<'static, Result<T, Err>> + Send + Sync + 'static>(
        initializer: Fun,
    ) -> Self {
        Self {
            inner: RwLock::new(None),
            initializer: Box::new(initializer),
        }
    }

    #[allow(clippy::missing_errors_doc)]
    /// Attempt to fetch the inner type the resurrectable holds. If it has not succesfully been
    /// initialized, this will attempt to initialize it first, returning an error if that fails.
    pub async fn get(&self) -> Result<LockHolder<'_, T>, Err> {
        let mut wl = self.inner.write().await;
        if wl.is_none() {
            *wl = Some((self.initializer)().await?);
        }
        drop(wl);
        // Guaranteed to be initialized here
        Ok(LockHolder {
            guard: self.inner.read().await,
        })
    }

    /// Returns true if the wrapped type has been initialized. IE: If this returns false, the next
    /// call to `get` must run the initializer, otherwise not.
    pub async fn is_initted(&self) -> bool {
        self.inner.read().await.is_some()
    }

    /// Resets the resurrectable such that the next `get` call will have to run the initializer
    pub async fn reset(&self) {
        (*self.inner.write().await) = None;
    }
}

pub struct ReconnectingXandApiClient {
    inner: Resurrectable<XandApiClient, XandApiClientError>,
}

impl ReconnectingXandApiClient {
    // TODO: Does not need to be async (nor should it) - fix with Mutex / other API changes.
    /// Create a new reconnecting client that has not even tried to connect. Will try to connect
    /// on the first call to `get`
    pub async fn stub(url: Url) -> Self {
        Self::stubber(url, None)
    }

    /// Like `stub`, but with a JWT to attach for authorization.
    #[must_use]
    pub fn stub_jwt(url: Url, jwt: String) -> Self {
        Self::stubber(url, Some(jwt))
    }

    fn stubber(url: Url, jwt: Option<String>) -> Self {
        Self {
            inner: Resurrectable::stub(move || {
                // These shouldn't *need* to be cloned... it is being passed in by ownership, but
                // async closures are wizardry still.
                let url = url.clone();
                let jwt = jwt.clone();
                async move {
                    let res = if let Some(jwt) = jwt {
                        XandApiClient::connect_with_jwt(&url, jwt).await
                    } else {
                        XandApiClient::connect(&url).await
                    };
                    if let Err(e) = &res {
                        warn!(ReconnectingClientError(format!("{:?}", e)))
                    };
                    res
                }
                .boxed()
            }),
        }
    }
}

#[async_trait::async_trait]
impl XandApiClientTrait for ReconnectingXandApiClient {
    async fn submit_transaction(
        &self,
        issuer: Address,
        txn: XandTransaction,
    ) -> Result<TransactionStatusStream, XandApiClientError> {
        self.inner
            .get()
            .await?
            .submit_transaction(issuer, txn)
            .await
    }

    async fn get_transaction_details(
        &self,
        id: &TransactionId,
    ) -> Result<Transaction, XandApiClientError> {
        self.inner.get().await?.get_transaction_details(id).await
    }

    async fn get_transaction_history(
        &self,
        paging: Option<Paging>,
        filter: &TransactionFilter,
    ) -> Result<Paginated<Vec<Transaction>>, XandApiClientError> {
        self.inner
            .get()
            .await?
            .get_transaction_history(paging, filter)
            .await
    }

    async fn get_balance(&self, address: &str) -> Result<Option<u128>, XandApiClientError> {
        self.inner.get().await?.get_balance(address).await
    }

    async fn get_current_block(&self) -> Result<Blockstamp, XandApiClientError> {
        self.inner.get().await?.get_current_block().await
    }

    async fn get_total_issuance(&self) -> Result<TotalIssuance, XandApiClientError> {
        self.inner.get().await?.get_total_issuance().await
    }

    async fn get_address_transactions(
        &self,
        address: &str,
        paging: Option<Paging>,
    ) -> Result<TransactionHistoryPage, XandApiClientError> {
        self.inner
            .get()
            .await?
            .get_address_transactions(address, paging)
            .await
    }

    async fn get_pending_create_requests(
        &self,
        paging: Option<Paging>,
    ) -> Result<Paginated<Vec<PendingCreateRequest>>, XandApiClientError> {
        self.inner
            .get()
            .await?
            .get_pending_create_requests(paging)
            .await
    }

    async fn get_pending_redeem_requests(
        &self,
        paging: Option<Paging>,
    ) -> Result<Paginated<Vec<PendingRedeemRequest>>, XandApiClientError> {
        self.inner
            .get()
            .await?
            .get_pending_redeem_requests(paging)
            .await
    }

    async fn propose_action(
        &self,
        issuer: Address,
        admin_txn: AdministrativeTransaction,
    ) -> Result<TransactionStatusStream, XandApiClientError> {
        self.inner
            .get()
            .await?
            .propose_action(issuer, admin_txn)
            .await
    }

    async fn vote_on_proposal(
        &self,
        issuer: Address,
        vote_proposal: VoteProposal,
    ) -> Result<TransactionStatusStream, XandApiClientError> {
        self.inner
            .get()
            .await?
            .vote_on_proposal(issuer, vote_proposal)
            .await
    }

    async fn get_proposal(&self, id: u32) -> Result<Proposal, XandApiClientError> {
        self.inner.get().await?.get_proposal(id).await
    }

    async fn get_all_proposals(&self) -> Result<Vec<Proposal>, XandApiClientError> {
        self.inner.get().await?.get_all_proposals().await
    }

    async fn get_members(&self) -> Result<Vec<Address>, XandApiClientError> {
        self.inner.get().await?.get_members().await
    }

    async fn get_authority_keys(&self) -> Result<Vec<Address>, XandApiClientError> {
        self.inner.get().await?.get_authority_keys().await
    }

    async fn get_trustee(&self) -> Result<Address, XandApiClientError> {
        self.inner.get().await?.get_trustee().await
    }

    async fn get_allowlist(&self) -> Result<Vec<(Address, CidrBlock)>, XandApiClientError> {
        self.inner.get().await?.get_allowlist().await
    }

    async fn get_limited_agent(&self) -> Result<Option<Address>, XandApiClientError> {
        self.inner.get().await?.get_limited_agent().await
    }

    async fn get_validator_emission_rate(
        &self,
    ) -> Result<ValidatorEmissionRate, XandApiClientError> {
        self.inner.get().await?.get_validator_emission_rate().await
    }

    async fn get_validator_emission_progress(
        &self,
        address: Address,
    ) -> Result<ValidatorEmissionProgress, XandApiClientError> {
        self.inner
            .get()
            .await?
            .get_validator_emission_progress(address)
            .await
    }

    async fn get_pending_create_request_expire_time(&self) -> Result<u64, XandApiClientError> {
        self.inner
            .get()
            .await?
            .get_pending_create_request_expire_time()
            .await
    }

    async fn check_health(&self) -> Result<HealthResponse, XandApiClientError> {
        self.inner.get().await?.check_health().await
    }
}

#[cfg(test)]
mod reconnector_tests {
    use super::*;
    use std::sync::Mutex;

    #[tokio::test]
    async fn resurrectable_works() {
        let responses: Mutex<Vec<Result<i32, ()>>> =
            Mutex::new(vec![Err(()), Ok(1), Err(()), Err(())]);
        let res = Resurrectable::new(move || {
            futures::future::ready(responses.lock().unwrap().pop().unwrap()).boxed()
        })
        .await;
        let inner_res = res.get().await;
        matches!(inner_res, Err(()));
        assert!(!res.is_initted().await);
        assert_eq!(*res.get().await.unwrap(), 1);
        assert!(res.is_initted().await);
        assert_eq!(*res.get().await.unwrap(), 1);
    }
}

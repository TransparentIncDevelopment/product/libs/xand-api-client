use crate::{models::Paginated, TransactionHistoryPage};
use std::convert::{TryFrom, TryInto};
use xand_api_proto::error::XandApiProtoErrs;
use xand_api_proto::proto_models::{PendingCreateRequest, PendingRedeemRequest};
use xand_api_proto::{PendingCreateRequests, PendingRedeemRequests, TransactionHistory};

impl TryFrom<PendingCreateRequests> for Paginated<Vec<PendingCreateRequest>> {
    type Error = XandApiProtoErrs;

    fn try_from(pc: PendingCreateRequests) -> Result<Self, Self::Error> {
        Ok(Paginated {
            data: pc
                .pending_create_requests
                .into_iter()
                .map(PendingCreateRequest::try_from)
                .collect::<Result<_, _>>()?,
            total: pc.total,
        })
    }
}

impl TryFrom<PendingRedeemRequests> for Paginated<Vec<PendingRedeemRequest>> {
    type Error = XandApiProtoErrs;

    fn try_from(pr: PendingRedeemRequests) -> Result<Self, Self::Error> {
        Ok(Paginated {
            data: pr
                .pending_redeem_requests
                .into_iter()
                .map(PendingRedeemRequest::try_from)
                .collect::<Result<_, _>>()?,
            total: pr.total,
        })
    }
}

impl TryFrom<TransactionHistory> for TransactionHistoryPage {
    type Error = XandApiProtoErrs;

    fn try_from(h: TransactionHistory) -> Result<Self, Self::Error> {
        Ok(Paginated {
            data: h
                .transactions
                .into_iter()
                .map(TryInto::try_into)
                .collect::<Result<Vec<_>, _>>()?,
            total: h.total,
        })
    }
}

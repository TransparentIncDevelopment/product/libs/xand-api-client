use crate::{
    Paginated, Paging, TransactionHistoryPage, TransactionStatusStream, TransactionUpdate,
    VoteProposal, XandApiClientError, XandApiClientTrait,
};
use async_trait::async_trait;
use futures::TryStreamExt;
use std::{future::Future, time::Duration};
use xand_address::Address;
use xand_api_proto::proto_models::{
    AdministrativeTransaction, Blockstamp, CidrBlock, HealthResponse, PendingCreateRequest,
    PendingRedeemRequest, Proposal, TotalIssuance, Transaction, TransactionFilter, TransactionId,
    ValidatorEmissionProgress, ValidatorEmissionRate, XandTransaction,
};

/// Wrapper type for any type implementing `XandAPIClientTrait` that adds a timeout to all requests.
#[derive(Debug)]
pub struct WithTimeout<T> {
    inner: T,
    timeout: Duration,
}

impl<T> WithTimeout<T> {
    /// Wraps a `XandAPIClientTrait` client and returns a client that applies the given timeout to
    /// all requests.
    pub fn new(client: T, timeout: Duration) -> Self {
        WithTimeout {
            inner: client,
            timeout,
        }
    }

    async fn timeout<F, O>(&self, task: F) -> F::Output
    where
        F: Future<Output = Result<O, XandApiClientError>>,
    {
        tokio::time::timeout(self.timeout, task)
            .await
            .unwrap_or(Err(XandApiClientError::Timeout))
    }
}

#[async_trait]
impl<T: XandApiClientTrait> XandApiClientTrait for WithTimeout<T> {
    async fn submit_transaction(
        &self,
        issuer: Address,
        txn: XandTransaction,
    ) -> Result<TransactionStatusStream, XandApiClientError> {
        self.timeout(self.inner.submit_transaction(issuer, txn))
            .await
    }

    async fn submit_transaction_wait(
        &self,
        issuer: Address,
        txn: XandTransaction,
    ) -> Result<TransactionUpdate, XandApiClientError> {
        let task = async move {
            self.inner
                .submit_transaction(issuer, txn)
                .await?
                .committed_result()
                .await
        };
        self.timeout(task).await
    }

    async fn submit_transaction_wait_final(
        &self,
        issuer: Address,
        txn: XandTransaction,
    ) -> Result<TransactionUpdate, XandApiClientError> {
        let task = async move {
            self.inner
                .submit_transaction(issuer, txn)
                .await?
                .try_fold(None, |_, update| async { Ok(Some(update)) })
                .await?
                .ok_or(XandApiClientError::UnknownTransactionStatus)
        };
        self.timeout(task).await
    }

    async fn get_transaction_details(
        &self,
        id: &TransactionId,
    ) -> Result<Transaction, XandApiClientError> {
        self.timeout(self.inner.get_transaction_details(id)).await
    }

    async fn get_transaction_history(
        &self,
        paging: Option<Paging>,
        filter: &TransactionFilter,
    ) -> Result<Paginated<Vec<Transaction>>, XandApiClientError> {
        self.timeout(self.inner.get_transaction_history(paging, filter))
            .await
    }

    async fn get_balance(&self, address: &str) -> Result<Option<u128>, XandApiClientError> {
        self.timeout(self.inner.get_balance(address)).await
    }

    async fn get_total_issuance(&self) -> Result<TotalIssuance, XandApiClientError> {
        self.timeout(self.inner.get_total_issuance()).await
    }

    async fn get_current_block(&self) -> Result<Blockstamp, XandApiClientError> {
        self.timeout(self.inner.get_current_block()).await
    }

    async fn get_address_transactions(
        &self,
        address: &str,
        paging: Option<Paging>,
    ) -> Result<TransactionHistoryPage, XandApiClientError> {
        self.timeout(self.inner.get_address_transactions(address, paging))
            .await
    }

    async fn get_pending_create_requests(
        &self,
        paging: Option<Paging>,
    ) -> Result<Paginated<Vec<PendingCreateRequest>>, XandApiClientError> {
        self.timeout(self.inner.get_pending_create_requests(paging))
            .await
    }

    async fn get_pending_redeem_requests(
        &self,
        paging: Option<Paging>,
    ) -> Result<Paginated<Vec<PendingRedeemRequest>>, XandApiClientError> {
        self.timeout(self.inner.get_pending_redeem_requests(paging))
            .await
    }

    async fn propose_action(
        &self,
        issuer: Address,
        admin_txn: AdministrativeTransaction,
    ) -> Result<TransactionStatusStream, XandApiClientError> {
        self.timeout(self.inner.propose_action(issuer, admin_txn))
            .await
    }

    async fn vote_on_proposal(
        &self,
        issuer: Address,
        vote_proposal: VoteProposal,
    ) -> Result<TransactionStatusStream, XandApiClientError> {
        self.timeout(self.inner.vote_on_proposal(issuer, vote_proposal))
            .await
    }

    async fn get_proposal(&self, id: u32) -> Result<Proposal, XandApiClientError> {
        self.timeout(self.inner.get_proposal(id)).await
    }

    async fn get_all_proposals(&self) -> Result<Vec<Proposal>, XandApiClientError> {
        self.timeout(self.inner.get_all_proposals()).await
    }

    async fn get_members(&self) -> Result<Vec<Address>, XandApiClientError> {
        self.timeout(self.inner.get_members()).await
    }

    async fn get_authority_keys(&self) -> Result<Vec<Address>, XandApiClientError> {
        self.timeout(self.inner.get_authority_keys()).await
    }

    async fn get_trustee(&self) -> Result<Address, XandApiClientError> {
        self.timeout(self.inner.get_trustee()).await
    }

    async fn get_allowlist(&self) -> Result<Vec<(Address, CidrBlock)>, XandApiClientError> {
        self.timeout(self.inner.get_allowlist()).await
    }

    async fn get_limited_agent(&self) -> Result<Option<Address>, XandApiClientError> {
        self.timeout(self.inner.get_limited_agent()).await
    }

    async fn get_validator_emission_rate(
        &self,
    ) -> Result<ValidatorEmissionRate, XandApiClientError> {
        self.timeout(self.inner.get_validator_emission_rate()).await
    }

    async fn get_validator_emission_progress(
        &self,
        address: Address,
    ) -> Result<ValidatorEmissionProgress, XandApiClientError> {
        self.timeout(self.inner.get_validator_emission_progress(address))
            .await
    }

    async fn get_pending_create_request_expire_time(&self) -> Result<u64, XandApiClientError> {
        self.timeout(self.inner.get_pending_create_request_expire_time())
            .await
    }

    async fn check_health(&self) -> Result<HealthResponse, XandApiClientError> {
        self.timeout(self.inner.check_health()).await
    }
}

#[cfg(test)]
mod tests {
    use crate::{mock::MockXandApiClient, XandApiClientError, XandApiClientTrait};
    use std::time::Duration;

    #[tokio::test]
    async fn timeout_is_applied() {
        let delay = Duration::from_secs(5);
        let timeout = Duration::from_millis(100);
        let client = MockXandApiClient::default()
            .with_delay(delay)
            .with_timeout(timeout);
        let result = client.get_pending_redeem_requests(None).await;
        assert!(matches!(result, Err(XandApiClientError::Timeout)));
    }
}

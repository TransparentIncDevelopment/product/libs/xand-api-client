use crate::errors::XandApiClientError;
use crate::{
    models::{Paginated, Paging, TransactionUpdate},
    Result, TransactionStatusStream, VoteProposal, XandApiClientTrait,
};
use futures::{stream, StreamExt};
use pseudo::Mock;
use std::time::Duration;
use xand_address::Address;
use xand_api_proto::proto_models::{
    AdministrativeTransaction, Blockstamp, CidrBlock, HealthResponse, PendingCreateRequest,
    PendingRedeemRequest, Proposal, TotalIssuance, Transaction, TransactionFilter, TransactionId,
    TransactionStatus, ValidatorEmissionProgress, ValidatorEmissionRate, XandTransaction,
};

type TxUpdateStream = Result<Vec<Result<TransactionUpdate>>>;

#[derive(Clone)]
#[allow(clippy::module_name_repetitions)]
pub struct MockXandApiClient {
    // A list of lists of transaction updates to reply with to calls to `submit_transaction`
    pub submits: Mock<(Address, XandTransaction), TxUpdateStream>,
    pub balance: Mock<String, Result<Option<u128>>>,
    pub pending_create_requests: Mock<(), Result<Paginated<Vec<PendingCreateRequest>>>>,
    pub pending_redeem_requests: Mock<(), Result<Paginated<Vec<PendingRedeemRequest>>>>,
    pub transaction_details: Mock<TransactionId, Result<Transaction>>,
    pub total_issuance: Mock<(), Result<TotalIssuance>>,
    pub delay: Option<Duration>,
}

impl MockXandApiClient {
    #[must_use]
    pub fn with_delay(self, delay: Duration) -> Self {
        Self {
            delay: Some(delay),
            ..self
        }
    }

    async fn pause(&self) {
        if let Some(d) = self.delay {
            tokio::time::sleep(d).await;
        }
    }
}

impl Default for MockXandApiClient {
    fn default() -> Self {
        MockXandApiClient {
            submits: Mock::new(Ok(Vec::default())),
            balance: Mock::new(Ok(Some(0_u128))),
            pending_create_requests: Mock::new(Ok(Paginated::default())),
            pending_redeem_requests: Mock::new(Ok(Paginated::default())),
            transaction_details: Mock::new(Err(XandApiClientError::NotFound { message: "Mock XandApiClient has not been configured to return information about this transaction.".to_string() })),
            total_issuance: Mock::new(Ok(TotalIssuance { total_issued: 0, blockstamp: Blockstamp {
                block_number: 0,
                unix_timestamp_ms: 0,
                is_stale: false
            } })),
            delay: None,
        }
    }
}

#[async_trait::async_trait]
impl XandApiClientTrait for MockXandApiClient {
    async fn submit_transaction(
        &self,
        address: Address,
        txn: XandTransaction,
    ) -> Result<TransactionStatusStream> {
        self.pause().await;
        let reply = self.submits.call((address, txn));
        if let Ok(reply) = reply {
            Ok(TransactionStatusStream {
                stream: stream::iter(reply).boxed(),
            })
        } else {
            Err(reply.unwrap_err())
        }
    }

    async fn get_transaction_details(&self, id: &TransactionId) -> Result<Transaction> {
        self.pause().await;
        self.transaction_details.call(id.clone())
    }

    async fn get_transaction_history(
        &self,
        _paging: Option<Paging>,
        _filter: &TransactionFilter,
    ) -> Result<Paginated<Vec<Transaction>>> {
        unimplemented!()
    }

    async fn get_balance(&self, address: &str) -> Result<Option<u128>> {
        self.pause().await;
        self.balance.call(address.to_string())
    }

    async fn get_total_issuance(&self) -> Result<TotalIssuance> {
        self.total_issuance.call(())
    }

    async fn get_address_transactions(
        &self,
        _address: &str,
        _paging: Option<Paging>,
    ) -> Result<Paginated<Vec<Transaction>>> {
        unimplemented!()
    }

    async fn get_current_block(&self) -> Result<Blockstamp> {
        unimplemented!()
    }

    async fn get_pending_create_requests(
        &self,
        _paging: Option<Paging>,
    ) -> Result<Paginated<Vec<PendingCreateRequest>>> {
        self.pause().await;
        self.pending_create_requests.call(())
    }

    async fn get_pending_redeem_requests(
        &self,
        _paging: Option<Paging>,
    ) -> Result<Paginated<Vec<PendingRedeemRequest>>> {
        self.pause().await;
        self.pending_redeem_requests.call(())
    }

    async fn propose_action(
        &self,
        _issuer: Address,
        _admin_txn: AdministrativeTransaction,
    ) -> Result<TransactionStatusStream, XandApiClientError> {
        unimplemented!()
    }

    async fn vote_on_proposal(
        &self,
        _issuer: Address,
        _vote_proposal: VoteProposal,
    ) -> Result<TransactionStatusStream, XandApiClientError> {
        unimplemented!()
    }

    async fn get_proposal(&self, _id: u32) -> Result<Proposal, XandApiClientError> {
        unimplemented!()
    }

    async fn get_all_proposals(&self) -> Result<Vec<Proposal>, XandApiClientError> {
        unimplemented!()
    }

    async fn get_members(&self) -> Result<Vec<Address>, XandApiClientError> {
        unimplemented!()
    }

    async fn get_authority_keys(&self) -> Result<Vec<Address>, XandApiClientError> {
        unimplemented!()
    }

    async fn get_trustee(&self) -> Result<Address, XandApiClientError> {
        unimplemented!()
    }

    async fn get_allowlist(&self) -> Result<Vec<(Address, CidrBlock)>, XandApiClientError> {
        unimplemented!()
    }

    async fn get_limited_agent(&self) -> Result<Option<Address>, XandApiClientError> {
        unimplemented!()
    }

    async fn get_validator_emission_rate(&self) -> Result<ValidatorEmissionRate> {
        unimplemented!()
    }

    async fn get_validator_emission_progress(
        &self,
        _address: Address,
    ) -> Result<ValidatorEmissionProgress> {
        unimplemented!()
    }

    async fn get_pending_create_request_expire_time(&self) -> Result<u64> {
        unimplemented!()
    }

    async fn check_health(&self) -> Result<HealthResponse> {
        unimplemented!()
    }
}

#[must_use]
pub fn committed_status() -> TransactionUpdate {
    TransactionUpdate {
        status: TransactionStatus::Committed,
        id: xand_api_proto::proto_models::TransactionId::default(),
    }
}

#[must_use]
pub fn finalized_status() -> TransactionUpdate {
    TransactionUpdate {
        status: TransactionStatus::Finalized,
        id: xand_api_proto::proto_models::TransactionId::default(),
    }
}

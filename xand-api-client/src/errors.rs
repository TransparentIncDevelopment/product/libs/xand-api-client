use serde::Serialize;
use snafu::Snafu;
use std::sync::Arc;
use tonic::{transport::Error, Code, Status};
use xand_api_proto::error::XandApiProtoErrs;
use xand_api_proto::proto_models::TransactionIdError;
// TODO consider changing snafu to thiserror
#[derive(Debug, Snafu, Clone, Serialize)]
#[snafu(visibility(pub))]
pub enum XandApiClientError {
    TransportError {
        #[snafu(source(from(tonic::transport::Error, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<tonic::transport::Error>,
    },
    #[snafu(display("Requested entity not found: {}", message))]
    NotFound {
        message: String,
    },
    #[snafu(display("Bad request: {}", message))]
    BadRequest {
        message: String,
    },
    /// Other grpc errors which are not represented as first-class items in this error enum
    OtherGrpcError {
        #[snafu(source(from(tonic::Status, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<tonic::Status>,
    },
    ProtoError {
        source: XandApiProtoErrs,
    },
    #[snafu(display("Transaction ID returned by xand-api is invalid: {}", source))]
    #[snafu(context(false))]
    InvalidIdInTransaction {
        source: TransactionIdError,
    },
    #[snafu(display("xand-api reply was missing required data: {}", message))]
    BadReplyError {
        message: String,
    },
    #[snafu(display("Transaction state is unknown"))]
    UnknownTransactionStatus,
    #[snafu(display("Invalid address {}", address))]
    InvalidAddress {
        address: String,
    },
    #[snafu(display("Invalid CIDR block {}", cidr_block))]
    InvalidCidrBlock {
        cidr_block: String,
    },
    #[snafu(display("The operation timed out"))]
    Timeout,

    #[snafu(display(
        "Your request was not authorized. Please check your credentials and try again: {}",
        message
    ))]
    Unauthorized {
        message: String,
    },
}

impl From<tonic::transport::Error> for XandApiClientError {
    fn from(e: Error) -> Self {
        XandApiClientError::TransportError {
            source: Arc::new(e),
        }
    }
}

impl From<tonic::Status> for XandApiClientError {
    fn from(e: Status) -> Self {
        match e.code() {
            Code::NotFound => XandApiClientError::NotFound {
                message: e.message().to_owned(),
            },
            Code::InvalidArgument => XandApiClientError::BadRequest {
                message: e.message().to_owned(),
            },
            Code::Unauthenticated => XandApiClientError::Unauthorized {
                message: e.message().to_owned(),
            },
            _ => XandApiClientError::OtherGrpcError { source: e.into() },
        }
    }
}

impl From<XandApiProtoErrs> for XandApiClientError {
    fn from(e: XandApiProtoErrs) -> Self {
        XandApiClientError::ProtoError { source: e }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    // XandApiClientError should be Send
    // Never is executed. Ignore clippy. Compile-only test.
    #[allow(unconditional_recursion, dead_code)]
    fn send_test<T: Send>(_: T) {
        send_test(XandApiClientError::BadRequest {
            message: "hi".to_string(),
        });
    }

    #[test]
    fn unauthorized() {
        let status = Status::new(Code::Unauthenticated, "some arbitrary message".to_string());
        let error: XandApiClientError = status.into();

        assert!(matches!(
            error,
            XandApiClientError::Unauthorized {
                message
            } if message == "some arbitrary message"
        ));
    }
}

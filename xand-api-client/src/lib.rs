//! Provides a trait and implementation of that trait for interacting with the `xand-api` (as
//! well as a fake version you can use in your tests in the `mock` module).
//!
//! If you are talking with `xand-api` over the wire, this is what you should use.

#![forbid(unsafe_code)]

#[macro_use]
extern crate tpfs_logger_port;

pub mod errors;
mod log_events;
pub mod mock;
pub mod models;
pub mod proto_help;
mod reconnector;
mod with_timeout;

// We re-export many things from xand-models so that consumers of the client can depend only on
// the client, and are in effect "told" what version of xand-models to use.
pub use reconnector::{ReconnectingXandApiClient, Resurrectable};
pub use tonic::{Code as TonicCode, Status as TonicStatus};
pub use with_timeout::WithTimeout;
pub use xand_address::{Address, AddressError};
pub use xand_api_proto::proto_models::*;

use crate::{
    errors::XandApiClientError,
    models::{Paginated, Paging, TransactionUpdate},
};
use futures::{stream::BoxStream, Stream, StreamExt, TryStreamExt};
use std::{
    convert::TryInto,
    fmt::Debug,
    pin::Pin,
    task::{Context, Poll},
    time::Duration,
};
use tonic::{codegen::http, transport::ClientTlsConfig, Streaming};
use url::Url;
use xand_api_proto::{
    self as xap, xand_api_client::XandApiClient as GrpcClient, TransactionUpdate as ProtoTxnUpdate,
};

type Result<T, E = XandApiClientError> = std::result::Result<T, E>;
pub type TransactionHistoryPage = Paginated<Vec<Transaction>>;

/// Constants for URL schemes
mod scheme {
    pub const HTTPS: &str = "https";
}

/// Trait that exposes all of the functionality provided by the xand-api as a simple client
/// interface
#[async_trait::async_trait]
pub trait XandApiClientTrait: std::marker::Send + Sync {
    /// Submit a transaction to the API, where it will be signed and then sent to the blockchain
    ///
    /// Returns a stream of transaction status updates
    async fn submit_transaction(
        &self,
        issuer: Address,
        txn: XandTransaction,
    ) -> Result<TransactionStatusStream>;

    /// Like `submit_transaction`, but rather than returning a stream, returns a future that
    /// completes once the the transaction has been committed or invalidated (not finalized).
    async fn submit_transaction_wait(
        &self,
        issuer: Address,
        txn: XandTransaction,
    ) -> Result<TransactionUpdate> {
        self.submit_transaction(issuer, txn)
            .await?
            .committed_result()
            .await
    }

    /// Submits a transaction and returns its final status (which, in the case of a valid
    /// transaction, should eventually be `Finalized`).
    ///
    /// You should generally prefer `submit_transaction_wait` over this, unless you must absolutely
    /// be sure the transaction was finalized (EX: you are the trust).
    async fn submit_transaction_wait_final(
        &self,
        issuer: Address,
        txn: XandTransaction,
    ) -> Result<TransactionUpdate> {
        self.submit_transaction(issuer, txn)
            .await?
            .try_fold(None, |_, update| async { Ok(Some(update)) })
            .await?
            .ok_or(XandApiClientError::UnknownTransactionStatus)
    }

    /// Fetch the details of a particular transaction
    async fn get_transaction_details(&self, id: &TransactionId) -> Result<Transaction>;

    /// Query transaction history
    async fn get_transaction_history(
        &self,
        paging: Option<Paging>,
        filter: &TransactionFilter,
    ) -> Result<Paginated<Vec<Transaction>>>;

    /// Fetch the balance (in USD cents) held by an address
    ///
    /// If the balance is unavailable (e.g. the requester cannot read it), `Ok(None)` is returned.
    ///
    /// TODO: Should also be `&Address`
    async fn get_balance(&self, address: &str) -> Result<Option<u128>>;

    async fn get_total_issuance(&self) -> Result<TotalIssuance>;

    /// Fetch all transactions which are relevant to the provided address
    async fn get_address_transactions(
        &self,
        address: &str,
        paging: Option<Paging>,
    ) -> Result<TransactionHistoryPage>;

    /// Request the current pending create requests
    async fn get_pending_create_requests(
        &self,
        paging: Option<Paging>,
    ) -> Result<Paginated<Vec<PendingCreateRequest>>>;

    /// Request the current pending redeem requests
    async fn get_pending_redeem_requests(
        &self,
        paging: Option<Paging>,
    ) -> Result<Paginated<Vec<PendingRedeemRequest>>>;

    /// Submit a proposal for an action to be voted on by network
    async fn propose_action(
        &self,
        issuer: Address,
        admin_txn: AdministrativeTransaction,
    ) -> Result<TransactionStatusStream>;

    /// Submit vote on specific proposal by id
    async fn vote_on_proposal(
        &self,
        issuer: Address,
        vote_proposal: VoteProposal,
    ) -> Result<TransactionStatusStream>;

    /// Gets the most current block and timestamp with a staleness indicator
    async fn get_current_block(&self) -> Result<Blockstamp>;

    /// Get specific proposal by id
    async fn get_proposal(&self, id: u32) -> Result<Proposal>;

    /// Get all non-expired proposals
    async fn get_all_proposals(&self) -> Result<Vec<Proposal>>;

    /// Get list of members
    async fn get_members(&self) -> Result<Vec<Address>>;

    /// Get list of validators
    async fn get_authority_keys(&self) -> Result<Vec<Address>>;

    /// Get trustee address
    async fn get_trustee(&self) -> Result<Address>;

    /// Get Limited Agent address if one exists
    async fn get_limited_agent(&self) -> Result<Option<Address>>;

    /// Get allowlisted CIDR blocks per address
    async fn get_allowlist(&self) -> Result<Vec<(Address, CidrBlock)>>;

    /// Get current Validator emission rate setting
    async fn get_validator_emission_rate(&self) -> Result<ValidatorEmissionRate>;

    /// Get the current progress of an individual validator toward an emission
    async fn get_validator_emission_progress(
        &self,
        address: Address,
    ) -> Result<ValidatorEmissionProgress>;

    /// Get the current pending create request expire time in milliseconds
    async fn get_pending_create_request_expire_time(&self) -> Result<u64>;

    /// Health check
    async fn check_health(&self) -> Result<HealthResponse>;

    /// Returns a client that behaves like `self` and applies the given timeout to every
    /// asynchronous operation.
    fn with_timeout(self, timeout: Duration) -> WithTimeout<Self>
    where
        Self: Sized,
    {
        WithTimeout::new(self, timeout)
    }
}

/// Wraps a stream of `TransactionStatus` updates to simplify common operations
#[derive(derive_more::Deref)]
pub struct TransactionStatusStream {
    stream: BoxStream<'static, Result<TransactionUpdate>>,
}

impl TransactionStatusStream {
    pub(crate) async fn committed_result(mut self) -> Result<TransactionUpdate> {
        let mut final_status = TransactionStatus::Unknown;
        let mut id = TransactionId::default();
        while let Some(update) = self.stream.next().await {
            let update = update?;
            id = update.id;
            match update.status {
                TransactionStatus::Invalid(_) | TransactionStatus::Committed => {
                    final_status = update.status;
                    break;
                }
                _ => continue,
            }
        }
        Ok(TransactionUpdate {
            status: final_status,
            id,
        })
    }
}

impl From<Vec<Result<TransactionUpdate>>> for TransactionStatusStream {
    fn from(u: Vec<Result<TransactionUpdate>>) -> Self {
        Self {
            stream: futures::stream::iter(u).boxed(),
        }
    }
}

impl From<Streaming<ProtoTxnUpdate>> for TransactionStatusStream {
    fn from(updates: Streaming<ProtoTxnUpdate>) -> Self {
        TransactionStatusStream {
            stream: updates
                .map(|update| {
                    let update = update?;
                    let id = update.id.parse()?;
                    let status = update.status.and_then(|s| s.status).ok_or_else(|| {
                        XandApiClientError::BadReplyError {
                            message: format!("Update for transaction {} had no status!", id),
                        }
                    })?;
                    Ok(TransactionUpdate {
                        id,
                        status: status.into(),
                    })
                })
                .boxed(),
        }
    }
}

impl Stream for TransactionStatusStream {
    type Item = Result<TransactionUpdate>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        self.stream.poll_next_unpin(cx)
    }
}

/// Default implementation of the `XandApiClientTrait`
pub struct XandApiClient {
    inner: GrpcClient<JwtIntercepted<tonic::transport::Channel>>,
}

impl XandApiClient {
    /// Returns xand-api client for given.
    pub async fn connect(url: &Url) -> Result<Self> {
        XandApiClient::connect_internal(url, None).await
    }

    /// Set up the client with a JWT that will be attached to all outgoing `Authorization` headers.
    pub async fn connect_with_jwt(url: &Url, jwt: String) -> Result<Self> {
        XandApiClient::connect_internal(url, Some(jwt)).await
    }

    /// Set up a connection to the given URL. Will negotiate TLS if the url scheme is "https".
    async fn connect_internal(url: &Url, jwt: Option<String>) -> Result<Self, XandApiClientError> {
        let conn = tonic::transport::Endpoint::new(url.to_string())?;
        let channel = if url.scheme().eq(scheme::HTTPS) {
            conn.tls_config(ClientTlsConfig::new())?
        } else {
            conn
        }
        .connect()
        .await?;
        let intercepted = JwtIntercepted {
            jwt,
            inner: channel,
        };
        Ok(XandApiClient {
            inner: GrpcClient::new(intercepted),
        })
    }
}

#[derive(Clone, Debug)]
struct JwtIntercepted<S> {
    jwt: Option<String>,
    inner: S,
}

impl<ReqBody, S> tower_service::Service<http::Request<ReqBody>> for JwtIntercepted<S>
where
    S: tower_service::Service<http::Request<ReqBody>>,
{
    type Response = S::Response;
    type Error = S::Error;
    type Future = S::Future;

    fn poll_ready(&mut self, ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(ctx)
    }

    fn call(&mut self, req: http::Request<ReqBody>) -> Self::Future {
        let req = match &self.jwt {
            Some(jwt) => attach_jwt(req, jwt),
            None => req,
        };
        self.inner.call(req)
    }
}

fn attach_jwt<T>(mut req: http::Request<T>, jwt: &str) -> http::Request<T> {
    let preamble = format!("Bearer {}", jwt);
    req.headers_mut().insert(
        http::header::AUTHORIZATION,
        http::HeaderValue::from_str(&preamble).unwrap(),
    );
    req
}

#[async_trait::async_trait]
impl XandApiClientTrait for XandApiClient {
    async fn submit_transaction(
        &self,
        issuer: Address,
        txn: XandTransaction,
    ) -> Result<TransactionStatusStream> {
        let resp = self
            .inner
            .clone()
            .submit_transaction(xap::UserTransaction {
                issuer: issuer.to_string(),
                operation: Some(txn.try_into()?),
            })
            .await?;
        Ok(resp.into_inner().into())
    }

    async fn get_transaction_details(&self, id: &TransactionId) -> Result<Transaction> {
        let resp = self
            .inner
            .clone()
            .get_transaction_details(xap::TransactionDetailsRequest { id: id.to_string() })
            .await?
            .into_inner();
        Ok(resp.try_into()?)
    }

    async fn get_current_block(&self) -> Result<Blockstamp> {
        let resp = self
            .inner
            .clone()
            .get_current_block(xap::GetCurrentBlockReq {})
            .await?
            .into_inner();
        Ok(resp.try_into()?)
    }

    async fn get_transaction_history(
        &self,
        paging: Option<Paging>,
        filter: &TransactionFilter,
    ) -> Result<Paginated<Vec<Transaction>>> {
        let paging = paging.unwrap_or_default();
        let resp = self
            .inner
            .clone()
            .get_transaction_history(xap::TransactionHistoryRequest {
                addresses: filter.addresses.iter().map(ToString::to_string).collect(),
                page_size: paging.page_size,
                page_number: paging.page_number,
                transaction_types: filter
                    .types
                    .iter()
                    .map(|&t| xand_api_proto::TransactionType::from(t) as i32)
                    .collect(),
                start_time: filter.start_time.map(|t| xap::Timestamp {
                    unix_time_millis: t.timestamp_millis(),
                }),
                end_time: filter.end_time.map(|t| xap::Timestamp {
                    unix_time_millis: t.timestamp_millis(),
                }),
            })
            .await?
            .into_inner();
        Ok(resp.try_into()?)
    }

    async fn get_balance(&self, address: &str) -> Result<Option<u128>> {
        let resp: xap::AddressBalance = self
            .inner
            .clone()
            .get_address_balance(xap::AddressBalanceRequest {
                address: address.to_string(),
            })
            .await?
            .into_inner();
        Ok(resp.balance.map(|b| b.amount.into()))
    }

    async fn get_total_issuance(&self) -> Result<TotalIssuance> {
        let resp: xap::TotalIssuanceResponse = self
            .inner
            .clone()
            .get_total_issuance(xap::TotalIssuanceRequest {})
            .await?
            .into_inner();
        Ok(resp.try_into()?)
    }

    async fn get_address_transactions(
        &self,
        address: &str,
        paging: Option<Paging>,
    ) -> Result<Paginated<Vec<Transaction>>> {
        let paging = paging.unwrap_or_default();
        let resp = self
            .inner
            .clone()
            .get_address_transactions(xap::AddressTransactionHistoryRequest {
                address: address.to_string(),
                page_size: paging.page_size,
                page_number: paging.page_number,
            })
            .await?
            .into_inner();
        Ok(resp.try_into()?)
    }

    async fn get_pending_create_requests(
        &self,
        paging: Option<Paging>,
    ) -> Result<Paginated<Vec<PendingCreateRequest>>> {
        let paging = paging.unwrap_or_default();
        let resp = self
            .inner
            .clone()
            .get_pending_create_requests(xap::PendingCreateRequestsPagination {
                page_size: paging.page_size,
                page_number: paging.page_number,
            })
            .await?;
        Ok(resp.into_inner().try_into()?)
    }

    async fn get_pending_redeem_requests(
        &self,
        paging: Option<Paging>,
    ) -> Result<Paginated<Vec<PendingRedeemRequest>>> {
        let paging = paging.unwrap_or_default();
        let resp = self
            .inner
            .clone()
            .get_pending_redeem_requests(xap::PendingRedeemRequestsPagination {
                page_size: paging.page_size,
                page_number: paging.page_number,
            })
            .await?;
        Ok(resp.into_inner().try_into()?)
    }

    async fn propose_action(
        &self,
        issuer: Address,
        proposed_action: AdministrativeTransaction,
    ) -> Result<TransactionStatusStream, XandApiClientError> {
        let resp = self
            .inner
            .clone()
            .propose_action(xap::SubmitProposal {
                issuer: issuer.to_string(),
                proposed_action: Some(proposed_action.into()),
            })
            .await?;
        Ok(resp.into_inner().into())
    }

    async fn vote_on_proposal(
        &self,
        issuer: Address,
        vote_proposal: VoteProposal,
    ) -> Result<TransactionStatusStream, XandApiClientError> {
        let resp = self
            .inner
            .clone()
            .vote_on_proposal(xap::VotingTransaction {
                issuer: issuer.to_string(),
                vote_proposal: Some(vote_proposal.into()),
            })
            .await?;
        Ok(resp.into_inner().into())
    }

    async fn get_proposal(&self, id: u32) -> Result<Proposal, XandApiClientError> {
        let resp = self
            .inner
            .clone()
            .get_proposal(xap::GetProposalReq { id })
            .await?
            .into_inner()
            .try_into()?;
        Ok(resp)
    }

    async fn get_all_proposals(&self) -> Result<Vec<Proposal>, XandApiClientError> {
        let resp = self
            .inner
            .clone()
            .get_all_proposals(xap::GetAllProposalsReq {})
            .await?
            .into_inner()
            .proposals
            .into_iter()
            .map(|p| p.try_into())
            .collect::<Result<Vec<_>, _>>()?;
        Ok(resp)
    }

    async fn get_members(&self) -> Result<Vec<Address>, XandApiClientError> {
        let members = self
            .inner
            .clone()
            .get_members(xap::MembersRequest {})
            .await?
            .into_inner()
            .members;
        to_addresses(members)
    }

    async fn get_authority_keys(&self) -> Result<Vec<Address>, XandApiClientError> {
        let keys = self
            .inner
            .clone()
            .get_authority_keys(xap::AuthorityKeysRequest {})
            .await?
            .into_inner()
            .authority_keys;
        to_addresses(keys)
    }

    async fn get_trustee(&self) -> Result<Address> {
        let trustee = self
            .inner
            .clone()
            .get_trustee(xap::GetTrusteeReq {})
            .await?
            .into_inner()
            .address;
        to_address(&trustee)
    }

    async fn get_allowlist(&self) -> Result<Vec<(Address, CidrBlock)>> {
        self.inner
            .clone()
            .get_allowlist(xap::AllowlistRequest {})
            .await?
            .into_inner()
            .entries
            .into_iter()
            .map(|entry| {
                Ok((
                    to_address(&entry.address)?,
                    to_cidr_block(&entry.cidr_block)?,
                ))
            })
            .collect()
    }

    async fn get_limited_agent(&self) -> Result<Option<Address>, XandApiClientError> {
        let limited_agent_address_option = self
            .inner
            .clone()
            .get_limited_agent(xap::GetLimitedAgentReq {})
            .await?
            .into_inner()
            .address;
        match limited_agent_address_option {
            Some(la) => Ok(to_address(&la.address_str).map(Some)?),
            None => Ok(None),
        }
    }

    async fn get_validator_emission_rate(&self) -> Result<ValidatorEmissionRate> {
        Ok(self
            .inner
            .clone()
            .get_validator_emission_rate(xap::GetValidatorEmissionRateReq {})
            .await?
            .into_inner()
            .into())
    }

    async fn get_validator_emission_progress(
        &self,
        address: Address,
    ) -> Result<ValidatorEmissionProgress> {
        Ok(self
            .inner
            .clone()
            .get_validator_emission_progress(xap::GetValidatorEmissionProgressReq {
                address: address.to_string(),
            })
            .await?
            .into_inner()
            .try_into()?)
    }

    async fn get_pending_create_request_expire_time(&self) -> Result<u64> {
        let resp: xap::PendingCreateRequestExpireTime = self
            .inner
            .clone()
            .get_pending_create_request_expire_time(xap::GetPendingCreateRequestExpireTimeReq {})
            .await?
            .into_inner();
        Ok(resp.into())
    }

    async fn check_health(&self) -> Result<HealthResponse> {
        let response: xap::HealthCheckResponse = self
            .inner
            .clone()
            .check_health(xap::HealthCheckRequest {})
            .await?
            .into_inner();
        Ok(response.try_into()?)
    }
}

fn to_addresses(v: Vec<String>) -> Result<Vec<Address>, XandApiClientError> {
    let addresses = v
        .into_iter()
        .map(|address| to_address(&address))
        .collect::<Result<Vec<_>, _>>()?;
    Ok(addresses)
}

fn to_address(s: &str) -> Result<Address, XandApiClientError> {
    #[allow(clippy::map_err_ignore)]
    s.parse()
        .map_err(|_| XandApiClientError::InvalidAddress { address: s.into() })
}

fn to_cidr_block(s: &str) -> Result<CidrBlock, XandApiClientError> {
    #[allow(clippy::map_err_ignore)]
    s.parse().map_err(|_| XandApiClientError::InvalidCidrBlock {
        cidr_block: s.into(),
    })
}

#[cfg(test)]
mod test {
    use super::*;
    use std::str::FromStr;

    #[ignore]
    #[tokio::test]
    async fn manual_test() {
        let mut client = GrpcClient::<tonic::transport::Channel>::connect("http://127.0.0.1:50051")
            .await
            .unwrap();
        let res = client
            .get_pending_create_requests(xap::PendingCreateRequestsPagination {
                page_size: 100,
                page_number: Default::default(),
            })
            .await
            .unwrap();
        dbg!(res);
    }

    // Is never executed. Ignore clippy.
    #[allow(unconditional_recursion, dead_code)]
    fn enforce_send<T: std::marker::Send>(_: T) {
        enforce_send(XandApiClient::connect(&Url::from_str("hi").unwrap()))
    }
}

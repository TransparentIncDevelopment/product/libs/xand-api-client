<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [xand-api-client](#xand-api-client)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# xand-api-client

A client for communicating with the `xand-api` over gRPC. Provides a more
ergonomic wrapper over the generated client.

It operates in terms of the client-side structs provided by `xand_api_proto::proto_models`.

For more details, see the rustdocs.

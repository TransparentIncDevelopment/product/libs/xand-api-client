[[_TOC_]]

# Contributing

## Changelog
When making changes to this repo, please make updates to the [CHANGELOG](CHANGELOG.md).

## Curators' Section

This section is to document ongoing design work or technical debt. If you have questions about any of these items please contact one of the Curators identified in this crate's CODEOWNERS file.

---
* **errors.rs**
   * This module should use `thiserror` instead of `snafu` for legibility (`snafu` has too much "hidden magic"). However, this involves porting over `From` annotations, and verifying that `thiserror` supports them. 

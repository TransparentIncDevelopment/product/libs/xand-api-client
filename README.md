# xand-api-client (repo)

This cargo workspace houses two closely related crates:

- [`xand-api-proto`](./xand-api-proto)
  - Defines protobuf wire types for the Xand API
- [`xand-api-client`](./xand-api-client)
  - Client that uses the Rust types generated from protobufs

Additionally, it owns the Typescript client, [`xand-api-ts-client`](./xand-api-ts-client).

## Versioning

Packages are published during CI on every merge to `master`.

As a policy, the version of the two crates and npm package must be the same. This is verified during CI.

To bump the package versions, run one of the following:

    - `./scripts/bump-version.sh major`
    - `./scripts/bump-version.sh minor`
    - `./scripts/bump-version.sh patch`

## Future Plans

These packages all originally came from the thermite monorepo and were extracted in an effort to reduce unnecessary coupling. We are considering eventually:

1. extracting the `*.proto` files into their own repo
1. migrating the modules from the `xand-api-proto` crate into `xand-api-client`
1. extracting `xand-api-ts-client` to its own repo

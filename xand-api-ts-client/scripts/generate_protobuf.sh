#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

BASEDIR="$(dirname "$0")"
PROTO_DIR="$(realpath ${BASEDIR}/../../xand-api-proto)"

cd "${BASEDIR}"/../

# Ensure `dist` dir exists. Grpc generator generates assets there
mkdir -p dist

# Also copy the protobuf files into dist so that they are accessible by consumers if needed
cp "${PROTO_DIR}"/*.proto dist/
cp -r "${PROTO_DIR}/google" dist/

PROTOC_GEN_TS_PATH="./node_modules/.bin/protoc-gen-ts"
GRPC_TOOLS_NODE_PROTOC_PLUGIN="./node_modules/.bin/grpc_tools_node_protoc_plugin"
GRPC_TOOLS_NODE_PROTOC="./node_modules/.bin/grpc_tools_node_protoc"

${GRPC_TOOLS_NODE_PROTOC} \
    --js_out=import_style=commonjs,binary:"./dist" \
    --grpc_out="./dist" \
    --plugin=protoc-gen-grpc="${GRPC_TOOLS_NODE_PROTOC_PLUGIN}" \
    -I "${PROTO_DIR}/" \
    "${PROTO_DIR}/xand-api.proto"

${GRPC_TOOLS_NODE_PROTOC} \
    --plugin=protoc-gen-ts="${PROTOC_GEN_TS_PATH}" \
    --ts_out="./dist" \
    -I "${PROTO_DIR}/" \
    "${PROTO_DIR}/xand-api.proto"

# google deps
${GRPC_TOOLS_NODE_PROTOC} \
    --js_out=import_style=commonjs,binary:"./google/api" \
    --grpc_out="./google/api" \
    --plugin=protoc-gen-grpc="${GRPC_TOOLS_NODE_PROTOC_PLUGIN}" \
    -I "./google/api" \
    ".//google/api/http.proto"

${GRPC_TOOLS_NODE_PROTOC} \
    --plugin=protoc-gen-ts="${PROTOC_GEN_TS_PATH}" \
    --ts_out="./google/api" \
    -I "./google/api" \
    "./google/api/http.proto"

${GRPC_TOOLS_NODE_PROTOC} \
    --js_out=import_style=commonjs,binary:"./google/api" \
    --grpc_out="./google/api" \
    --plugin=protoc-gen-grpc="${GRPC_TOOLS_NODE_PROTOC_PLUGIN}" \
    -I "./google/api" \
    "./google/api/annotations.proto"

${GRPC_TOOLS_NODE_PROTOC} \
    --plugin=protoc-gen-ts="${PROTOC_GEN_TS_PATH}" \
    --ts_out="./google/api" \
    -I "./google/api" \
    "./google/api/annotations.proto"

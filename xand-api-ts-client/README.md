<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [XAND API gRPC TS Client](#xand-api-grpc-ts-client)
  - [Generate protobufs and typescript client](#generate-protobufs-and-typescript-client)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

XAND API gRPC TS Client
===

A (mostly) auto-generated client for the gRPC implementation of the Xand API.

Generate protobufs and typescript client
--

`yarn build`

